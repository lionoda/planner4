'use strict'; 

window.planner = window.planner || {}; 

planner.Utils = class {

	sanitizeString(str){
	  str = str.replace(/[^a-z0-9áéíóúñü \.,_-]/gim,"");
	  return str.trim();
	}

}

planner.utils = new planner.Utils();