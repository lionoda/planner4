'use strict';  

window.planner = window.planner || {}; 

planner.Trip = class {

	constructor() {
		this.tripEndpoint = '/api/trip/';

		$('body').on('click', '.header-conv-box .user-input-button', (event) => {
			let location = $('.header-conv-box-question .header-title').text();

			planner.router.updateComponents({ 'component-home': 'active', 'component-single-trip': 'passive' }, true); 
			planner.home.addTripStickyFooter({ title: $('.trip-detail-title .title').text(), _id: window.tripId }); 
			planner.home.getCategoryForConv(location, $(event.currentTarget).data('category')); 
			
			$('.conv-box-one').addClass('conv-box-passive'); 
			$('.conv-box-two').removeClass('conv-box-hidden').addClass('animated fadeInUp');
			$('.conv-box-two .location-name').empty().append(location);

			setTimeout(() => {
				$('.conv-box-three').removeClass('conv-box-hidden').addClass('animated fadeInUp')
				$('.conv-box-three--before').removeClass('conv-box-hidden').addClass('animated fadeInUp')
			}, 500);

		})
	}

	loadTrip(tripId) {
		window.tripId = tripId; 
		let self = this;
		async.waterfall([
			function(callback) {
				$.ajax({
					url: self.tripEndpoint + tripId, 
					method: 'GET' 
				})
				.done((trip) => {
					console.log('trip: ', trip);
					callback(false, trip); 
				})
				.fail(() => {
					callback(500);
				})
			}, 
			function(trip, callback) {
				self.addTripToDom(trip.trip); 

				$('.timeline-day-content').empty(); 
				for(let i = 0; trip.venues.length > i; i++) {
					self.addVenueToDOM(trip.venues[i]); 
				}

				callback(false, trip._id);
			}
		])
	}

	addTripToDom(trip) {
		$('.header-conv-box-question .header-title').append(trip.area);
		$('.trip-detail-title .title').empty().append(trip.title); 
	}

	addVenueToDOM(venue) {
		const venuePhoto = venue.venue.featuredPhotos.items[0].prefix + '300x300' + venue.venue.featuredPhotos.items[0].suffix; 

		let venueAddress = ''; 
		for(let j = 0; venue.venue.location.formattedAddress.length > j; j++) {
			venueAddress = venueAddress + ' ' + venue.venue.location.formattedAddress[j]; 
		}

		let venueReviewImage = venue.tips[0].user.photo.prefix + '50x50' + venue.tips[0].user.photo.suffix; 
		let venueReview = venue.tips[0].text;

		$(`.timeline-content`).append(`
			<div class="row">
				<div class="col-md-9 col-sm-9">
					<div class="trip-card full-width background-gray pull-left">
						<div class="pull-left xs-full-width">
							<div class="trip-card-image" style="background-image: url(${venuePhoto})"></div>
						</div>
						<div class="pull-left trip-card-description">
							<div class="pull-left full-width">
								<div class="trip-card-title black">${venue.venue.name}</div>
							</div>
							<div class="pull-left venue-description-address">
								<div class="pull-left venue-description-address-icon"></div>
								<div class="pull-left venue-description-address-label gray">${venueAddress}</div>
							</div>
							<div class="pull-left full-width">
								<div class="venue-description-review-image pull-left" style="background-image: url(${venueReviewImage})"></div>
								<div class="trip-card-review-label pull-left">${venueReview}</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-offset-1 col-md-2 col-sm-offset-1 col-sm-2">
					<div class="background-gray full-width pull-left venue-edit-menu">
						<div class="pull-left delete-trip-item-trigger full-width" data-value="${venue._id}" data-day="${venue.venue.day}">
							<div class="delete-trip-item-icon"></div>
							<div class="delete-trip-item-label">Delete</div>
						</div>
					</div>
				</div>
			</div>
		`);

	}

}

planner.trip = new planner.Trip(); 