'use strict'; 

window.planner = window.planner || {}; 


planner.Home = class {

	constructor() {

		this.venueEndpoint = '/api/venue/';
		this.tripEndpoint = '/api/trip/';

		// Conv box step one
		$('body').on('submit', '.conv-box-one--form', (event) => {
			event.preventDefault(); 
			$('.conv-box-one').addClass('conv-box-passive');
			$('.conv-box-two').removeClass('conv-box-hidden').addClass('animated fadeInUp');
			$('.conv-box-one input').attr('disabled', 'disabled');
			$('.conv-box-one select').attr('disabled', 'disabled');

			let location = $('input.user-input-text[name="destination"]').val(); 

			$('.conv-box-two .location-name').empty().append(location);
		})

		// Conv box step two
		$('body').on('click', '.conv-user-input .user-input-button', (event) => {

			$('.conv-box-three--results').empty(); 
			$('.user-input-button').removeClass('user-input-button-active');
			$('.conv-box-three--before-form').empty();

			if($(event.currentTarget).hasClass('user-input-button-active')) {
				$(event.currentTarget).removeClass('user-input-button-active');
			} else {
				$(event.currentTarget).addClass('user-input-button-active');
			}

			let category = $(event.currentTarget).data('category');
			let nearby = $('.location-name').text();
			this.getCategoryForConv(nearby, category); 

			setTimeout(() => {
				$('.conv-box-three').removeClass('conv-box-hidden').addClass('animated fadeInUp')
				$('.conv-box-three--before').removeClass('conv-box-hidden').addClass('animated fadeInUp')
			}, 500);
		})

		$('body').on('click', '.conv-box-subcategory-button', (event) => {
			if($(event.currentTarget).hasClass('conv-box-subcategory-button-active')) {
				$(event.currentTarget).removeClass('conv-box-subcategory-button-active');
			} else {
				$(event.currentTarget).addClass('conv-box-subcategory-button-active');
			}

			let category = $(event.currentTarget).data('category');
			let parentCategory = $(event.currentTarget).data('parent-category');
			let nearby = $('.location-name').text();
			console.log('parent 3: ', parentCategory);
			this.getCategoryForConv(nearby, category, parentCategory); 
		})

		$('body').on('click', '.conv-results-add-box', (event) => {
			let venueId = $(event.currentTarget).data('venue-id'); 
			let tripId = window.tripId; 
			let tripTitle = $('.conv-box-two .location-name').text(); 
			this.addVenueToTrip(venueId, tripId, tripTitle); 
		})

		$('body').on('click', '.view-trip-button', (event) => {
			planner.router.updateComponents({ 'component-home': 'passive', 'component-single-trip': 'active' }, true); 
			$('.sticky-trip-footer').hide();
			$('.conv-box-three--results').empty(); 
			$('.conv-box-three--before').removeClass('animated fadeInUp').addClass('conv-box-hidden');
			$('.conv-box-three--before-form').empty();
		})

		$('body').on('click', '.user-input-button-search', (event) => {
			if(!$(event.currentTarget).hasClass('user-input-button-search-active')) {
				$(event.currentTarget).addClass('user-input-button-search-active'); 
				$(event.currentTarget).append(`<input type="text"/>`);
				$('.user-input-button-search-active input').focus();
			}
		})

		$('body').on('click', '.conv-box-one.conv-box-passive', (event) => {
			$('.conv-box-one').removeClass('conv-box-passive');
			$('.conv-box-one input').removeAttr('disabled');
			$('.conv-box-one select').removeAttr('disabled');
			$('.conv-box-two').addClass('conv-box-hidden');
			$('.conv-box-three--results').empty();
			$('.conv-box-three--before').removeClass('animated fadeInUp').addClass('conv-box-hidden');
			$('.conv-box-three--before-form').empty();
		})

	}

	getCategoryForConv(nearby, category, parent) {
		console.log('parent 1', parent);
		let self = this; 
		$.ajax({
			url: this.venueEndpoint + 'search/' + nearby + '/' + category, 
			method: 'GET', 
			beforeSend: () => {
				$('.conv-box-three--results').append(`<div class="conv-box-three--results-section" data-category="${category}">
																								<div class="col-md-4 col-xs-12 mb-conv-result-card-box">
																									<div class="pull-left result-dummy-card background-gray full-width"></div>
																								</div>
																								<div class="col-md-4 col-xs-12 mb-conv-result-card-box">
																									<div class="pull-left result-dummy-card background-gray full-width"></div>
																								</div>
																								<div class="col-md-4 col-xs-12 mb-conv-result-card-box">
																									<div class="pull-left result-dummy-card background-gray full-width"></div>
																								</div>
																							</div>`)
			}
		})
		.done((msg) => {
			console.log(msg);
			this.prepareCategoryForConv(msg, category, parent); 
		})
		.fail(() => {
			// show error message
		})
	}

	prepareCategoryForConv(msg, category, parent) {

		let venues = msg.venues; 
		let subCategories = msg.subCategories; 

		console.log('parent? ', parent);
		if(parent) {
			$(`.conv-box-subcategory-button[data-parent-category="${parent}"]`).remove();
		}

		subCategories = subCategories.slice(0, 5); 

		for(let subCategory in subCategories) {
			$('.conv-box-three--before-form').append(`<div class="pull-left conv-box-subcategory-button" data-parent-category="${category}" data-category="${subCategories[subCategory][0]}">${subCategories[subCategory][0]}</div>`); 
		}

		// Remove all dummy cards
		$(`.conv-box-three--results-section[data-category="${category}"]`).find('.result-dummy-card').parent().remove();

		// Remove all parent cards
		if(parent) {
			$(`.conv-box-three--results-section[data-category="${parent}"]`).empty();
		}

		for(let i = 0; venues.length > i; i++) {
			this.addVenueToConv(venues[i], category); 
		}
	}

	addVenueToConv(venue, category) {

		async.waterfall([
			function(callback) {

				const venuePhoto = venue.venue.featuredPhotos.items[0].prefix + '500x500' + venue.venue.featuredPhotos.items[0].suffix; 

				let venueReviewImage = venue.tips[0].user.photo.prefix + '50x50' + venue.tips[0].user.photo.suffix; 
				let venueReview = venue.tips[0].text;

				let venueAddress = ''; 
				for(let j = 0; venue.venue.location.formattedAddress.length > j; j++) {
					venueAddress = venueAddress + ' ' + venue.venue.location.formattedAddress[j]; 
				}

				if(venue.venue.url) {
					const venueWebsite = venue.venue.url;  
					const venueWebsiteLabel = 'Company website';
					callback(false, venuePhoto, venueAddress, venueReviewImage, venueReview, venueWebsite, venueWebsiteLabel)
				} else {
					const venueWebsite = 'https://foursquare.com/v/' + venue.fqVenueId; 
					const venueWebsiteLabel = 'More information';
					callback(false, venuePhoto, venueAddress, venueReviewImage, venueReview, venueWebsite, venueWebsiteLabel)
				}

				
			}, 
			function(venuePhoto, venueAddress, venueReviewImage, venueReview, venueWebsite, venueWebsiteLabel, callback) {
				$(`.conv-box-three--results-section[data-category="${category}"]`).append(`
					<div class="col-md-4 col-xs-12 mb-conv-result-card-box">
						<div class="pull-left background-gray full-width conv-result-card">
							<div class="row">
								<div class="col-md-12">
									<div class="conv-results-background" style="background-image: url(${venuePhoto})"></div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="conv-results-title pull-left">${venue.venue.name}</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="conv-results-address pull-left">
										<div class="pull-left conv-results-address-icon"></div>
										<div class="pull-left conv-results-address-label gray">${venueAddress}</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="conv-results-website pull-left">
										<div class="pull-left conv-results-website-icon"></div>
										<a class="conv-results-website-label gray pull-left no-decoration" target="_blank" href="${venueWebsite}">
											${venueWebsiteLabel}
										</a>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="conv-results-review pull-left">
										<div class="pull-left conv-results-review-image" style="background-image: url('${venueReviewImage}')"></div>
										<div class="pull-left conv-results-review-label black">${venueReview}</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="conv-results-add-box full-width pull-left" data-venue-id="${venue._id}">
										<div class="background-white full-width conv-results-add">
											<div class="conv-results-add-icon"></div>
											<div class="conv-results-add-label darkgreen">Add to trip</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				`)

				callback();
			}
		])
		
	}

	createTrip(callback) {

		let amountOfDays = $('select[name="duration"]').find(':selected').data('value'); 
		console.log('amountOfDays: ', amountOfDays); 
		let startDate = moment([2016, $('select[name="timeframe"]').find(':selected').data('value')]).add('month');
		let finishDate = moment(startDate).endOf('month'); 
		let area = $('.location-name').text();
		let title = area + ' in ' + 'February'; 

		$.ajax({
			url: this.tripEndpoint, 
			method: 'POST',
			data: { title: title, 
							startDate: startDate.toDate(), 
							finishDate: finishDate.toDate(), 
							amountOfDays: amountOfDays, 
							area: area
			}
		})
		.done((msg) => {
			callback(false, msg); 
		})
		.fail((err) => {
			callback(err); 
		})
	}

	postVenueToTrip(tripId, venueId, callback) {
		$.ajax({
			url: this.tripEndpoint + 'add/' + tripId + '/' + venueId, 
			method: 'POST'
		})
		.done((venue) => {
			callback(false, venue); 
		})
		.fail((err) => {
			callback(err); 
		})
	}

	addVenueToTrip(venueId, tripId, tripTitle) {

		let url = false; 
		let self = this;

		console.log('addVenueToTrip venueId: ', venueId); 

		async.waterfall([
			function(callback) {
				if(!tripId) {
					self.createTrip((err, trip) => {
						console.log('err: ', err, trip);
						callback(err, trip._id);
					}); 
				} else {
					console.log('window trip id found', window.tripId);
					callback(false, tripId);
				}
			}, 
			function(tripId, callback) {
				self.postVenueToTrip(tripId, venueId, function(err, venue) {
					if(err) {
						callback(err); 
					} else {
						callback(false, tripId, venue); 
					}
				})
			}, function(tripId, venue, callback) {
				if(window.location.href.indexOf('/trip/') === -1) {
					page.redirect(`/trip/${tripId}`);
				}
				self.addTripStickyFooter({ _id: tripId, title: tripTitle }); 
				
				callback(false, venue); 
				// Check if trip in dom already exists
				//   --> Redirect to it
				//   --> Create bottom thingy
			}, function(venue, callback) {
				// Append venue to trip
				//   --> Create the animation

				$(`.conv-results-add-box[data-venue-id="${venueId}"]`).closest('.conv-result-card').addClass('conv-result-card-animate');
			}
		])
		

	
	}

	addTripStickyFooter(trip) {
		console.log('trip: ', trip);
		if(!$('body').hasClass('sticky-trip-footer')) {
			$('body').append(`<div class="sticky-trip-footer background-darkgreen animated fadeInLeft">
													<div class="container">
														<div class="row">
															<div class="col-md-12">
																<div class="sticky-trip-footer-content pull-left">
																	<p class="white">${trip.title}</p>
																</div>
																<div class="pull-right">
																	<div class="background-white black pull-left view-trip-button" data-trip-id="${trip._id}">View trip</div>
																</div>
															</div>
														</div>
													</div>
												</div>
			`);
		}
	}

}

planner.home = new planner.Home();