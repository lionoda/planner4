'use strict'; 

window.planner = window.planner || {}; 

planner.Router = class {
		
	constructor() {
		$(document).ready(() => {

			this.pageComponents = $('[id^=component-]');

			const pipe = planner.Router.pipe; 
			const displayPage = this.displayPage.bind(this); 
			const viewTrip = tripId => planner.trip.loadTrip(tripId); 
			const initComponents = components => this.updateComponents(components, false); 

			page('/', pipe(initComponents, { 'component-home': 'active', 'component-single-trip': 'passive' }) );
			page('/trip/:tripId', pipe(viewTrip, null, true), pipe(initComponents, { 'component-home': 'passive', 'component-single-trip': 'active' }) ); 
			page('*', () => page('/'));
			page(); 
		})
	}

	displayPage(attributes, context) {

		// Page exists out of components
		let components = attributes.components;

		this.pageComponents.each(function(index, element) {
			console.log('components: ', components, element.id);
			if(_.contains(components, element.id)) {
				$(element).show(); 
			} else if(element.id === 'component-splash') {
				$(element).fadeOut(1000); 
			} else {
				$(element).hide(); 
			}
		})
		planner.Router.scrollToTop(); 
	}
   

	static pipe(funct, attribute, optContinue) {
    return (context, next) => {
      if (funct) {
        const params = Object.keys(context.params);
        if (!attribute && params.length > 0) {
          funct(context.params[params[0]], context);
        } else {
          funct(attribute, context);
        }
      }
      if (optContinue) {
        next();
      }
    };
  }

  static scrollToTop() {
    $('html,body').animate({scrollTop: 0}, 0);
  }

  updateComponents(components, force) {

  	console.log('components: ', components, force);

  	for(let component in components) {

  		if(force) {
	  		console.log('component: ', component, components[component], force);
	  		$(`#${component}`).removeClass('animated slideOutDown slideOutUp slideInDown slideInUp');

	  		if($(`#${component}`).hasClass('component-active') && $(`#${component}`).hasClass('single-trip-component')) {
	  			// Toggle to passive

	  			// Slide down
	  			$(`#${component}`).addClass('animated slideOutDown'); 
	  		} 

	  		if($(`#${component}`).hasClass('component-active') && $(`#${component}`).hasClass('home-component')) {
	  			// Toggle to passive

	  			// Slide up
	  			$(`#${component}`).addClass('animated slideOutUp'); 
	  		}

	  		if($(`#${component}`).hasClass('component-passive') && $(`#${component}`).hasClass('single-trip-component')) {
	  			// Toggle to active

	  			// Slide up
	  			$(`#${component}`).addClass('animated slideInUp');
	  		}

	  		if($(`#${component}`).hasClass('component-passive') && $(`#${component}`).hasClass('home-component')) {
	  			// Toggle to active
	  			console.log('whyyy?', component);
	  			// Slide down
	  			$(`#${component}`).addClass('animated slideInDown');
	  		}
	  	}


  		if( !$(`#${component}`).hasClass('component-loaded') || force) {
  			if(force) {
  				$(`#${component}`).removeClass('component-passive component-active').addClass(`component-loaded component-${components[component]}`);
  			} else {
  				$(`#${component}`).removeClass('component-passive component-active').addClass(`component-loaded component-${components[component]}`);
  			}
  		}

  	}
  }

}

planner.router = new planner.Router();