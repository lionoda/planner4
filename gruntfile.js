module.exports = function(grunt){

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.initConfig({
      uglify: {
        options: {
          sourceMap: false,
          wrap: 'exports',
          mangle: false
        },
        my_target: {
          files: {
            './public/planner.bundle.min.js': './public/dist/planner.bundle.js'
          }
        }
      }, 
      watch: {
        js: {
          files: ['./public/dist/planner.bundle.js'], 
          tasks: ['uglify']
        }
      }
    });

    grunt.registerTask('default', ['uglify']);
}