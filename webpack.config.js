module.exports = {
	entry: ['./public/js/utils.js', './public/js/single-trip-component.js', './public/js/home-component.js', './public/js/routing.js'], 
	module: {
	  loaders: [
	    {
	      test: /\.js$/,
	      exclude: /(node_modules|bower_components)/,
	      loader: 'babel',
	      query: {
	        presets: ['es2015']
	      }
	    }
	  ]
	}, 
	output: {
    path: './public/dist',
    filename: 'planner.bundle.js'
  }
};