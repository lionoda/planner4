var mongoose = require('mongoose'); 

var Schema = mongoose.Schema, 
	ObjectId = Schema.ObjectId; 

var tripSchema = new Schema({ 
	createdAt: { type: Date, default: Date.now }, 
	lastUpdated: { type: Date, default: Date.now }, 
	owner: ObjectId, 
	title: String, 
	tripItems: Array, // [day: Number, venue: Object Id] 
	status: String, 
	startDate: Date, 
	finishDate: Date, 
	amountOfDays: Number, 
	area: String
})

module.exports = mongoose.model('Trip', tripSchema); 