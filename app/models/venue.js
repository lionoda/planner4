var mongoose = require('mongoose'); 

mongoose.set('debug', true);

var Schema = mongoose.Schema, 
	ObjectId = Schema.ObjectId; 

var venueSchema = new Schema({
	venue: ObjectId, 
	createdAt: { type: Date, default: Date.now }, 
	lastUpdated: { type: Date, default: Date.now }, 
	reasons: Object, 
	tips: Object, 
	referralId: String, 
	venue: Object, 
	assignedTo: Object, 
	owner: String
})

module.exports = mongoose.model('Venue', venueSchema); 