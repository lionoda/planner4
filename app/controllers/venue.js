var config = require('../../config/config'); 
var request = require('request'); 
var qs = require('qs'); 
var _ = require('underscore'); 
var async = require('async'); 
var Venue = require('../models/venue');

var exploreVenues = function(req, callback) {

	var query = req.query; 
	var near = req.near; 
	var limit = req.limit || 6; 
	var offset = req.offset || 0; 
	var fqBaseVars = config.fqBaseVars; 

	fqRequest = {}; 
	fqRequest = _.extend(fqRequest, fqBaseVars); 
	fqRequest = _.extend(fqRequest, { query: query, near: near, venuePhotos: 1 }); 

	var venueExploreEndPoint = (process.env.fqBaseEndpoint || config.fqBaseEndpoint ) + (process.env.fqExploreVenueEndpoint || config.fqExploreVenueEndpoint) + '?' + qs.stringify(fqRequest);

	console.log('request endpoint: ', venueExploreEndPoint); 

	request({ url: venueExploreEndPoint, headers: { 'Accept-Language': 'en' } }, callback);

}

var getVenues = function(req, callback) {
	var owner = req.owner || 'public'; 
	var identifiers = req.identifiers; 
	var query = req.query; 
	var near = req.near;

	async.waterfall([
		function(callback) {
			exploreVenues({ near: near, query: query }, function(err, response, venues) {
				if(err) {
					callback(err); 
				} else {

					venues = JSON.parse(venues); 
					if(venues.response.groups) {
						callback(false, venues.response.groups[0].items); 
					} else {
						callback('not-found'); 
					}

				}
			})
		}, 
		function(venues, callback) {
			var fqIdentifiers = []; 
			var subCategories = {}; 
			async.each(venues, function(venue, innerCallback) {

				for(var i = 0; venue.venue.categories.length > i; i++) {
					var venueCategories = venue.venue.categories[i];

					if(venueCategories.pluralName !== query) {
						var pluralNameArray = subCategories[venueCategories.pluralName] ? subCategories[venueCategories.pluralName] : 0; 
						subCategories[venueCategories.pluralName] = pluralNameArray + 1; 
					}
				}

				Venue.update({ fqVenueId: venue.venue.id }, venue, { upsert: true }, function(err, result) {
					fqIdentifiers.push(venue.venue.id); 
					innerCallback();
				})
			}, function(err) {
				if(err) {
					callback(err)
				} else {

					// Order categories by max value
					var sortableCategories = [];
					for(var subCategory in subCategories) {
						sortableCategories.push([subCategory, subCategories[subCategory] ])
						sortableCategories.sort(
							function(a, b) {
								return b[1] - a[1]
							}
						)
					}

					callback(false, fqIdentifiers, sortableCategories);
				}
			})
		}, 
		function(fqIdentifiers, subCategories, callback) {
			Venue.find({ 'fqVenueId': { '$in': fqIdentifiers } }, function(err, docs) {
				if(err) {
					callback(err)
				} else {
					callback(false, docs, subCategories); 
				}
			})
		}
	], function(err, venues, subCategories) {
		if(err) {
			console.error('ERROR: ', err); 
			callback(err); 
		} else {
			callback(false, venues, subCategories); 
		}

	})
}

var getVenue = function(identifier, callback) {
	var query = {}; 
	query[identifier.key] = identifier.value;
	Venue.findOne(query, function(err, venue) {
		if(err) {
			callback(err)
		} else {
			callback(false, venue); 
		}
	})
}

module.exports = { getVenues: getVenues, getVenue: getVenue }; 