var Trip = require('../models/trip'); 
var Venue = require('../models/venue');
var _ = require('underscore');

var createTrip = function(req, callback) {

	console.log('tripdata: ', req);

	var tripData = new Trip(); 
	tripData.title = req.title; 
	tripData.startDate = req.startDate; 
	tripData.finishDate = req.finishDate; 
	tripData.amountOfDays = req.amountOfDays; 
	tripData.area = req.area; 

	tripData.save(function(err) {
		if(err) {
			callback(err); 
		} else {
			callback(false, tripData); 
		}
	})
}

var addVenueToTrip = function(tripId, venueId, callback) {
	console.log('tripId: ', tripId);
	Trip.update({ _id: tripId }, { '$addToSet': { tripItems: venueId } }, function(err, result) {
		if(err) {
			callback(err); 
		} else {
			callback(); 
		}
	})

}

var getTrip = function(identifier, callback) {
	var query = {}; 
	query[identifier.key] = identifier.value; 
	Trip.findOne(query, function(err, trip) {
		if(err) {
			callback(err); 
		} 

		var tripItems = trip.tripItems; 
		if(tripItems) {
			Venue.find({ _id: { $in: tripItems } }, function(err, venues) {
				if(err) {
					callback(err); 
				} else {
					var tripData = {}; 
					tripData.venues = venues; 
					tripData.trip = trip; 
					callback(false, tripData); 
				}
			})

		} else {
			callback(false, { trip: trip }); 
		}
	})
}

module.exports = { createTrip: createTrip, addVenueToTrip: addVenueToTrip, getTrip: getTrip }; 