var express    = require('express');        
var app        = express();                
var bodyParser = require('body-parser');

var config 		 = require('./config/config');
var path    	 = require('path');
var async 		 = require('async'); 
var mailgun 	 = require('mailgun-js')({apiKey: 'key-4839f4382172e01e21d942d596cec70a', domain: 'mg.tripsally.com'});
var _ 				 = require('underscore');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('public'));

var tripController = require('./app/controllers/trip'); 
var venueController = require('./app/controllers/venue'); 

var mongoose = require('mongoose');
mongoose.connect(process.env.MONGO_URL || config.mongoEndpoint); 


var router = express.Router(); 

var APIRouter = express.Router();

app.use('/api', APIRouter);

app.use('/', router); 
app.set('views', __dirname + '/app/views');


router.route('/')
	.get(function(req, res) {
		res.sendFile(path.join(__dirname+'/app/views/index.html'));
	})

router.route('/trip/:tripId')
	.get(function(req, res) {
		res.sendFile(path.join(__dirname+'/app/views/index.html'));
	})

APIRouter.route('/venue/search/:near/:query')
	.get(function(req, res) {
		venueController.getVenues({ near: req.params.near, query: req.params.query}, function(err, venues, subCategories) {
			if(err) {
				console.log('error: ', err); 
				res.sendStatus(500); 
			} 

			if(venues && subCategories) { 
				res.send({ subCategories: subCategories, venues: venues });
			}
		}); 
	})

APIRouter.route('/trip/add/:tripId/:venueId')
	.post(function(req, res) {
		tripController.addVenueToTrip(req.params.tripId, req.params.venueId, function(err) {
			if(err) {
				res.sendStatus(500);
			} else {
				venueController.getVenue({ key: '_id', value: req.params.venueId }, function(err, venue) {
					if(err) {
						res.sendStatus(500); 
					} else {
						res.send(venue); 
					}
				})
			}
		});
	})

APIRouter.route('/trip')
	.post(function(req, res) {
		tripController.createTrip({ title: req.body.title, startDate: req.body.startDate, finishDate: req.body.finishDate, amountOfDays: req.body.amountOfDays, area: req.body.area }, function(err, trip) {
			if(err) {
				console.log('error: ', err); 
				res.sendStatus(500); 
			}

			if(trip) {
				console.log('trip created!'); 
				res.send(trip); 
			}
		})
	})

APIRouter.route('/trip/:tripId')
	.get(function(req, res) {
		tripController.getTrip({ key: '_id', value: req.params.tripId }, function(err, trip) {
			if(err) {
				res.sendStatus(500); 
			} else {
				res.send(trip); 
			}
		})
	})


var port = process.env.PORT || 8080;
app.listen(port);
console.log('Server listening on port ' + port);